var Lname = document.getElementById(Lname);
var Fname = document.getElementById(Fname);
var bd = document.getElementById(bd);
var httpRequest;

function makeRequest() {
    var bt = document.getElementById('bt'); 

    httpRequest = new XMLHttpRequest();

    var btValue  = bt.value;
    var battleTag = btValue.replace('#','-');
    var url = 'https://ow-api.com/v1/stats/pc/eu/' + battleTag + '/profile';
    
    if (!httpRequest) {
        alert('Abandon :( Impossible de créer une instance de XMLHTTP');
        return false;
    }

    httpRequest.onreadystatechange = alertContents;
    
    httpRequest.open('GET', url, true);
    httpRequest.send();
}

function alertContents() {
    if (httpRequest.readyState === XMLHttpRequest.DONE) {

        if (httpRequest.status === 200) {
            const obj = JSON.parse(httpRequest.response);
            console.log(obj)
            
            var name = obj.name;
            var level = obj.level;
            var gamesPlayed = obj.quickPlayStats.games.played;
            var victories = obj.quickPlayStats.games.won;
            var lostGames = gamesPlayed - victories;
            document.getElementById("result").innerHTML =  name + " is Level: " + level + " and has played "+gamesPlayed+" games.<br/>Number of victories : "+victories+". <br/>Number of lost games : "+lostGames+".";
           
        } else {
        alert('Il y a eu un problème avec la requête.');
        }
    }
}
